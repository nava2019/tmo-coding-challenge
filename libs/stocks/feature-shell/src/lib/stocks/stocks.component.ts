import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { DataPersistence } from '@nrwl/nx';
import { element } from '@angular/core/src/render3';
import { Observable, observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {
  stockPickerForm: FormGroup;
  symbol: string;
  period: string;
  fullData: any;
  quotes$ = this.priceQuery.priceQueries$;

  timePeriods = [
    { viewValue: 'All available data', value: 'max' },
    { viewValue: 'Five years', value: '5y' },
    { viewValue: 'Two years', value: '2y' },
    { viewValue: 'One year', value: '1y' },
    { viewValue: 'Year-to-date', value: 'ytd' },
    { viewValue: 'Six months', value: '6m' },
    { viewValue: 'Three months', value: '3m' },
    { viewValue: 'One month', value: '1m' }
  ];

  maxDate = new Date();
  toSelectedDate: Date;
  showRange = false;
  buttonContent = 'Show filters';
  invalidDate = false;

  constructor(private fb: FormBuilder, private priceQuery: PriceQueryFacade) {
    this.stockPickerForm = fb.group({
      symbol: [null, Validators.required],
      period: [null, Validators.required],
      startDate: [null],
      endDate: [null]
    });
  }

  ngOnInit() {
    this.stockPickerForm.valueChanges.subscribe(quote => { 
      this.fetchQuote();
    });
  }

  fetchQuote() {
    if (this.stockPickerForm.valid) {
      const { symbol, period, startDate, endDate} = this.stockPickerForm.value;
      if (startDate && endDate && this.showRange) {
        this.priceQuery.fetchQuote(symbol, '', startDate, endDate);
      }
      else if (period) {
      this.priceQuery.fetchQuote(symbol, period);
      }
    }
  }


  TodateInput(event: MatDatepickerInputEvent<Date>) {
    this.invalidDate = false;
    this.toSelectedDate = event.value;
    if (this.toSelectedDate < this.stockPickerForm.value.startDate) {
      this.stockPickerForm.get('endDate').setValue(this.stockPickerForm.value.startDate);
      this.invalidDate = true;
    }
  }

  
  changeButton() {
    if (this.buttonContent === 'Show filters') {
      this.buttonContent = 'Hide filters';
      this.showRange = true;
    } else {
      this.buttonContent = 'Show filters';
      this.showRange = false;
    }
    this.updatePeriodValidity();
  }

  updatePeriodValidity() {  
    const period = this.stockPickerForm.get('period');
    const startDate = this.stockPickerForm.get('startDate');
    const endDate = this.stockPickerForm.get('endDate');
    if (this.showRange) {
      period.setValue(null);
      period.setValidators(null);
      startDate.setValidators(Validators.required);
      endDate.setValidators(Validators.required);
    } else {
      period.setValidators(Validators.required);
      startDate.setValue(null);
      endDate.setValue(null);
      startDate.setValidators(null);
      endDate.setValidators(null);
    }
    period.updateValueAndValidity();
    startDate.updateValueAndValidity();
    endDate.updateValueAndValidity();
  }
}
