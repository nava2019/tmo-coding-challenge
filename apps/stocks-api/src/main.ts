/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
import { Server } from 'hapi';

const request = require('request');

const init = async () => {
    const server = new Server({
        port: 3333,
      host: 'localhost',
    });
server.route({
           method: 'GET',
           path: '/',
           handler: async (req, reply) => {
                 const symbol = 'AAPL';
                 const timePeriod = '1M';
                 const token='Tpk_a55ae444ca2442e1ab74fad967c769ac';
                 const response = server.methods.getdata(symbol,timePeriod,token); 
                 return response;
             
            }
        });

        const getdata = function (symbol, timePeriod, token) {
            const url = 'https://sandbox.iexapis.com/beta/stock/' + symbol + '/chart/' + timePeriod + '?token=' + token;
            alert(url);
             return new Promise((resolve, reject) => {
                request(url, (error, response, body) => {
                  if (response && response['statusCode'] === 200) {
                    resolve(body);
                  } 
                });
            });
        };

       

      server.method('getdata', getdata, {
        cache: {
            expiresIn: 10*1000,
            generateTimeout: 200
        },
        generateKey: (symbol, timePeriod) => symbol + '_' + timePeriod
    });
       
       
  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
